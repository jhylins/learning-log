## VSCode

This is a list of my most used keyboard shortcuts (mac). 
A list of all shortcuts can be found in the Keyboard Shortcuts preferences <kbd>command</kbd><kbd>K</kbd><kbd>command</kbd><kbd>S</kbd>  
If you already know a keyboard shortcut and want to find it in VSCode's long list of shortcuts, use 'record keys' to find it in the list.

### Code Navigation

<kbd>option</kbd><kbd>►</kbd> Move cursor one word to the right  

### Code Selection

<kbd>shift</kbd><kbd>control</kbd><kbd>command</kbd><kbd>►</kbd> Expand selection  
<kbd>command</kbd><kbd>d</kbd> Select word  

### Basic Code Editing

<kbd>option</kbd><kbd>backspace</kbd> Delete word left  
<kbd>shift</kbd><kbd>command</kbd><kbd>k</kbd> Delete line  

### Editors

<kbd>control</kbd><kbd>command</kbd><kbd>►</kbd> (Whilst in editor) move to new group on right  
<kbd>command</kbd><kbd>W</kbd> Close editor  
<kbd>command</kbd><kbd>K</kbd><kbd>command</kbd><kbd>W</kbd> Close all editors  
<kbd>option</kbd><kbd>command</kbd><kbd>T</kbd> Close all other editors  

### Extensions

<kbd>control</kbd><kbd>shift</kbd><kbd>q</kbd> Create console log (with console-log extension)  
<kbd>shift</kbd><kbd>option</kbd><kbd>f</kbd> Format code

## MacOS

<kbd>command</kbd><kbd>delete</kbd> Move to bin