## Housekeeping and Audit of Dependencies

Run npm outdated, and npm audit to see what issues exist in your dependencies.

To see where those dependencies are in the tree you can run `npm ls -all`, or `npm ls <package>` to trace it's location in the dependency tree.

```bash
npm audit
npm outdated
npm ls <package>
npm update <package>
```